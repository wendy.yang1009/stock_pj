# # 股票数据更新Python3
## 重点
本工程使用[Tushare大数据社区](https://tushare.pro/)接口以及雪球、同花顺接口，全部功能需要 Tushare 5000积分(500块钱)，雪球接口需要不定时手动更新cookie信息,如果无法接受Tushare的积分计价规则，可自己调整获取股票数据的接口，本人不会协助修改成其他接口，忘见谅

## 开始
stock_pj 项目是个人量化选股中摘取出来的每日股票数据更新到本地数据库的Python代码，代码内线程的控制可根据电脑配置自行设置最优化的方案，楼主目前电脑是macBook Pro M1 8+256,配置20个线程进行更新，A股4800+代码数据更新完毕，耗时在6分钟左右

## 准备
环境配置
* [mongodb](https://www.mongodb.com/)
* python3
* pip 校验是否安装以下库(列表中个别人实际上此项目用不到，但是可能在量化选股中用到，由于楼主比较懒，没有单独做梳理，这里建议全部安装，耗时并不长)
```python
async-generator    1.10
attrs              21.4.0
beautifulsoup4     4.11.1
browser-cookie3    0.14.1
bs4                0.0.1
certifi            2021.10.8
cffi               1.15.0
charset-normalizer 2.0.12
cryptography       37.0.2
cssselect          1.1.0
easyquotation      0.7.5
easyutils          0.1.7
et-xmlfile         1.1.0
h11                0.13.0
idna               3.3
importlib-metadata 4.11.3
jeepney            0.8.0
keyring            23.5.0
lxml               4.8.0
lz4                4.0.0
numpy              1.22.3
openpyxl           3.0.9
outcome            1.1.0
pandas             1.4.2
pbkdf2             1.3
phantomjs          1.4.1
pip                22.1
pyaes              1.6.1
pycparser          2.21
pycryptodome       3.14.1
PyExecJS           1.5.1
pymongo            4.1.1
pyOpenSSL          22.0.0
pyquery            1.4.3
PySocks            1.7.1
python-dateutil    2.8.2
pytz               2022.1
requests           2.27.1
SecretStorage      3.3.2
selenium           4.1.5
setuptools         58.1.0
simplejson         3.17.6
six                1.16.0
sniffio            1.2.0
sortedcontainers   2.4.0
soupsieve          2.3.2.post1
trio               0.20.0
trio-websocket     0.9.2
tushare            1.2.85
urllib3            1.26.9
websocket-client   0.57.0
wencai             0.2.6
wheel              0.37.1
wsproto            1.1.0
zipp               3.8.0

```

## 执行
* 进去到stock_py/SDaily目录
* 执行以下命令既可
```python
python tudaily.py
```

## 完毕
更新完成会出现类似如下日志
```python
更新 000920.SZ 沃顿科技 第 4816 - 4808 个 耗时 0.27958083152770996
更新 002395.SZ 双象股份 第 4816 - 4809 个 耗时 0.29534292221069336
更新 002149.SZ 西部材料 第 4816 - 4810 个 耗时 0.28939175605773926
更新 000561.SZ 烽火电子 第 4816 - 4811 个 耗时 0.3862271308898926
更新 000921.SZ 海信家电 第 4816 - 4812 个 耗时 0.2449800968170166
更新 002150.SZ 通润装备 第 4816 - 4813 个 耗时 0.23929095268249512
更新 000563.SZ 陕国投A 第 4816 - 4814 个 耗时 0.2530038356781006
更新 002151.SZ 北斗星通 第 4816 - 4815 个 耗时 0.31407594680786133
更新 000564.SZ *ST大集 第 4816 - 4816 个 耗时 0.2594640254974365
更新完成 总耗时 321.2190661668777
```
## 文件说明
* 默认执行上面的命令更新股票日信息，如果想更新股票板块信息，打开中间的注释代码既可，注释代码更新的是同花顺行业二三级，此功能需要[Tushare大数据社区](https://tushare.pro/) 5000积分,其他股票更新数据，2000积分既可
* Tushare参数配置：StockSingle 文件中找到 set_token,设置自己的token
```python
class StockSingle:
    _instance = None
    _flag = True
    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__new__(cls)
        return cls._instance 

    def __init__(self):
        if StockSingle._flag:
            self._uindex=0
            self.haoshi=0
            self._trade_date_now=''
            self.trade_stock_now_file_path=''
            self.f = None
            ts.set_token('**********')
            self.pro = ts.pro_api()
            StockSingle._flag = False


```

* 雪球参数配置：StockRequest文件中查找xq_a_token替换成自己的cookie，可以用浏览器登录雪球获取cookie中的xq_a_token
```python
class StockRequest:
    def __init__(self):
        self.dateTimp = str(int(time.time()*1000)) 
        self.header = {
            'cookie':'xq_is_login=1;xq_a_token=**********',
            'User-Agent': 'Xueqiu iPhone 13.6.5'
        }

```


## 数据库数据说明
```python
{
    "ts_code":"000001.SZ",#股票代码
    "symbol":"000001",#股票代码
    "name":"平安银行",#股票名称
    "trade_daily":[
        {
            "trade_date":"20220602", #交易日期
            "vol":97926822, #交易量
            "open":14.02,#开盘价
            "high":14.03,#最高价
            "low":13.88,#最低价
            "close":13.95,#收盘价
            "ptc_chg":-0.13,#忘了
            "percent":-0.92,#涨跌幅
            "hanshou":0.5,#换手
            "amount":1363740797,#交易额
            "isYearHigh":0,#是否一年新高
            "is20High":0,#是否20日新高
            "is60High":0,#是否60日新高
            "is120High":0,#是否120日新高
            "is20Wave":0,#20日振幅是否小于10%
            "avail_5":-0.0169,#5日涨跌幅
            "avail_20":-0.0894,#20日涨跌幅
            "avail_60":-0.112,#60日涨跌幅
            "avail_250":-0.4061,#250日涨跌幅
            "net_mf_amount":"-21017.68万",#净流入额
            "buy_elg_amount":"31593.47万",#特大单额
            "buy_elg_percent":"28.00%",#特大单占比
            "limit_status":0#是否涨停
        }
    ],#日K数据，差不多在360多条
    "list_date_days":11383,#上市天数
    "trade_date":"2022-06-02",#最新交易日期
    "total_mv":2707.12558862,#总市值
    "circ_mv":2707.07031203,#流通市值
    "limit_status":0,#是否涨停
    "hk_ORG":16300054290,#北向持仓
    "jijin_ORG":4.46,#基金持仓
    "qfii_ORG":0,#qfii持仓
    "shebao_ORG":0,#设保持仓
    "hy":"银行",#二级行业
    "hy2":"股份制银行",#三级行业行业
    "hy3":"",#无
    "list_status":1#是否交易、停牌等
}

```
